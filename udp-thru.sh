#!/bin/sh
# performs UDP speed tests with a narrowing window to find the maximum throughput
#

[ "$1" = '-h' ]  && echo "Usage: $0 [server] [D|U] [min] [max]" && exit

#thresh is how narrow two consecutive results should be before finishing
thresh=2000

server=$1
direction=$2
min_rate=$3
max_rate=$4

min_rate=${min_rate:-$NATTCP_MIN_RATE}
max_rate=${max_rate:-$NATTCP_MAX_RATE}
server=${server:-$NATTCP_SERVER}
direction=${direction:-$NATTCP_DIRECTION}

min_rate=${min_rate:-100}
max_rate=${max_rate:-20000}
server=${server:-184.106.215.57}
direction=${direction:-D}

abs_min_rate=$min_rate
abs_max_rate=$max_rate

[ "$direction" = D ] || [ "$direction" = U ] || [ "$direction" = d ] || [ "$direction" = u ] || direction=D

run_test() {
  local min=$1
  local max=$2
  local step=$3
  local test_result=$(udp-climber.lua -f $min -t $max -s $step -T 1 -$direction --no-ssl $server | tee -a /tmp/thru.out)
  # example output 1: Best result was: 24.229600mbps/24229.600000kbps/23661kibits
  # example output 2: No peformance decrease detectable. Please rerun with limit > 24102kbps.
  # example output 3: Receive rate does no longer increase or there are excessive drops
  if echo $test_result | grep -q "No peformance decrease"; then
    echo $max && return
  fi
  if echo $test_result | grep -q "no longer increase"; then
    loss=$(echo $test_result | grep -B1 "no longer increase" | sed "s/.*loss = \(.*\)%.*/\1/")
    [ $loss = 0 ] && maxed='m'
  fi
  test_result=$(echo $test_result | grep -o "Best.*" || echo fail)
  [ "$test_result" = "fail" ] && exit 3
  test_result=${test_result#*/}
  test_result=${test_result%%.*}
  test_result=${test_result%%k*}
  echo $maxed$test_result
}

while [ true ]; do
  rate_step=$(( ($max_rate-$min_rate)/3 ))
  test_result=$(run_test $min_rate $max_rate $rate_step)
  test_result_maxed=${test_result%%[^m]*}
  test_result=${test_result##m}
  test_result=$((test_result*1))
  [ $test_result = 0 ] && rate_step=${prev_rate_step:-$rate_step}
  [ $test_result -ge $abs_max_rate ] && echo $max_rate && exit
  [ $(($max_rate - $test_result)) -lt $rate_step ] && rate_step=${prev_rate_step:-$rate_step}
  if [ "x$test_result_maxed" = "xm" ]; then
    [ $(($max_rate - $test_result )) -gt $thresh ] && break
    if [ $prev_maxed ]; then
      [ $prev_maxed -lt $test_result ] && [ $(( $test_result - $prev_maxed )) -lt $thresh ] && break
      [ $test_result -lt $prev_maxed ] && [ $(( $prev_maxed - $test_result )) -lt $thresh ] && break
    fi
    prev_maxed=$test_result
    min_rate=$test_result
  else
    min_rate=$(($test_result-$rate_step/2))
  fi
  [ $min_rate -lt $abs_min_rate ] && min_rate=$abs_min_rate
  max_rate=$(($test_result+$rate_step/2))
  [ $(($max_rate-$min_rate)) -lt $thresh ] && break
  [ $max_rate -gt $abs_max_rate ] && max_rate=$abs_max_rate
  prev_rate_step=$rate_step
done

[ $test_result -ne 0 ] && echo $test_result || echo $min_rate
